package test.com.retail.core;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.retail.core.CalculateGroundCost;
import com.retail.core.Item;
@RunWith(Parameterized.class)
public class CalculateGroundCostTest {
	private double expectedValue;
	private Item item;
	public CalculateGroundCostTest(double expectedValue, Item item) {
		super();
		this.expectedValue = expectedValue;
		this.item = item;
	}
	CalculateGroundCost groundCost;
	@Before
	public void intiate() {
		groundCost= new CalculateGroundCost();
	}

	@Test
	public void test() {
		assertEquals(expectedValue, groundCost.groundCost(item),0.01);
		
	}
	@Parameters
	public static Collection<Object[]> testData(){
		Object[][] data = new Object[][] {{1.53,new Item(567321101986L , "CD � Beatles, Abbey Road" , 17.99f, 0.61 , " GROUND")},
			{1.25,new Item(567321101984L , "CD � Michael Jackson, Thriller" , 23.88f, 0.50 , " GROUND")},
			{8.03,new Item(477321101878L , "iPhone -  Headphones" , 17.25f, 3.21 , " GROUND")}};
			return Arrays.asList(data);	
		}
	}


