package test.com.retail.core;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.retail.core.CalculateRailCost;
import com.retail.core.Item;

@RunWith(Parameterized.class)
public class CalculateRailCostTest {

	private double expectedValue;
	private Item item;
	
	CalculateRailCost railCost;
	@Before
	public void intiate() {
		railCost= new CalculateRailCost();
	}
	
	public CalculateRailCostTest(double expectedValue, Item item) {
		this.expectedValue = expectedValue;
		this.item = item;
	}
	
	
	@Test
	public void test() {
		assertEquals(expectedValue, railCost.railShippingCost(item),0.01);
	}
	@Parameters
	public static Collection<Object[]> testData(){
		Object[][] data = new Object[][] { 
			{10.0, new Item(312321101516L,"Hot Tub",9899.99f,793.41,"RAIL")},
				{5.0,new Item(322322202488L,"HeavyMac Laptop",4555.79f,4.08,"RAIL")} };

	return Arrays.asList(data);	
	}
		
}
